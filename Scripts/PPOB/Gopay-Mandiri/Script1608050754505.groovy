import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://staging.ladara.id/login#!')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.navigateToUrl('https://staging.ladara.id/')

WebUI.click(findTestObject('PPOB/Page_/div_Gopay'))

WebUI.setText(findTestObject('Object Repository/PPOB/Page_/input_Nomor Telepon_customerNumber'), '081286281318')

WebUI.selectOptionByLabel(findTestObject('PPOB/Page_/select_nominal'), 'Rp 20000', false)

WebUI.verifyOptionPresentByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), 'Rp 20000', false, 60)

WebUI.click(findTestObject('Object Repository/PPOB/Page_/button_Topup'))

WebUI.delay(3)

WebUI.click(findTestObject('PPOB/Page_Ladara/button_Pilih Metode Pembayaran'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('PPOB/Page_Ladara/div_Bank Mandiri'))

WebUI.delay(3)

WebUI.click(findTestObject('PPOB/Page_Ladara/button_Bayar Sekarang'))

WebUI.delay(3)

WebUI.click(findTestObject('PPOB/Page_Ladara/Page_Ladara/a_DEV Simulasi pembayaran'))

WebUI.delay(3)

WebUI.navigateToUrl('https://staging.ladara.id/')

WebUI.click(findTestObject('PPOB/Page_/div_Gopay'))

WebUI.setText(findTestObject('Object Repository/PPOB/Page_/input_Nomor Telepon_customerNumber'), '081286281318')

WebUI.selectOptionByLabel(findTestObject('PPOB/Page_/select_nominal'), 'Rp 50000', false)

WebUI.verifyOptionPresentByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), 'Rp 50000', false, 60)

WebUI.click(findTestObject('PPOB/Page_/button_Topup'))

WebUI.click(findTestObject('PPOB/Page_Ladara/button_Pilih Metode Pembayaran'))

WebUI.click(findTestObject('PPOB/Page_Ladara/div_Bank Mandiri'))

WebUI.click(findTestObject('PPOB/Page_Ladara/button_Bayar Sekarang'))

WebUI.click(findTestObject('PPOB/Page_Ladara/Page_Ladara/a_DEV Simulasi pembayaran'))

WebUI.navigateToUrl('https://staging.ladara.id/')

WebUI.click(findTestObject('PPOB/Page_/div_Gopay'))

WebUI.setText(findTestObject('Object Repository/PPOB/Page_/input_Nomor Telepon_customerNumber'), '081286281318')

WebUI.selectOptionByLabel(findTestObject('PPOB/Page_/select_nominal'), 'Rp 100000', false)

WebUI.verifyOptionPresentByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), 'Rp 100000', false, 60)

WebUI.click(findTestObject('PPOB/Page_/button_Topup'))

WebUI.click(findTestObject('PPOB/Page_Ladara/button_Pilih Metode Pembayaran'))

WebUI.click(findTestObject('PPOB/Page_Ladara/div_Bank Mandiri'))

WebUI.click(findTestObject('PPOB/Page_Ladara/button_Bayar Sekarang'))

WebUI.click(findTestObject('PPOB/Page_Ladara/Page_Ladara/a_DEV Simulasi pembayaran'))

WebUI.navigateToUrl('https://staging.ladara.id/')

WebUI.click(findTestObject('PPOB/Page_/div_Gopay'))

WebUI.setText(findTestObject('Object Repository/PPOB/Page_/input_Nomor Telepon_customerNumber'), '081286281318')

WebUI.selectOptionByLabel(findTestObject('PPOB/Page_/select_nominal'), 'Rp 200000', false)

WebUI.verifyOptionPresentByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), 'Rp 200000', false, 60)

WebUI.click(findTestObject('PPOB/Page_/button_Topup'))

WebUI.click(findTestObject('PPOB/Page_Ladara/button_Pilih Metode Pembayaran'))

WebUI.click(findTestObject('PPOB/Page_Ladara/div_Bank Mandiri'))

WebUI.click(findTestObject('PPOB/Page_Ladara/button_Bayar Sekarang'))

WebUI.click(findTestObject('PPOB/Page_Ladara/Page_Ladara/a_DEV Simulasi pembayaran'))

WebUI.navigateToUrl('https://staging.ladara.id/')

WebUI.click(findTestObject('PPOB/Page_/div_Gopay'))

WebUI.setText(findTestObject('Object Repository/PPOB/Page_/input_Nomor Telepon_customerNumber'), '081286281318')

WebUI.selectOptionByLabel(findTestObject('PPOB/Page_/select_nominal'), 'Rp 500000', false)

WebUI.verifyOptionPresentByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), 'Rp 500000', false, 60)

WebUI.click(findTestObject('PPOB/Page_/button_Topup'))

WebUI.click(findTestObject('PPOB/Page_Ladara/button_Pilih Metode Pembayaran'))

WebUI.click(findTestObject('PPOB/Page_Ladara/div_Bank Mandiri'))

WebUI.click(findTestObject('PPOB/Page_Ladara/button_Bayar Sekarang'))

WebUI.click(findTestObject('PPOB/Page_Ladara/Page_Ladara/a_DEV Simulasi pembayaran'))

WebUI.navigateToUrl('https://staging.ladara.id/')

WebUI.click(findTestObject('PPOB/Page_/div_Gopay'))

WebUI.setText(findTestObject('Object Repository/PPOB/Page_/input_Nomor Telepon_customerNumber'), '081286281318')

WebUI.selectOptionByLabel(findTestObject('PPOB/Page_/select_nominal'), 'Rp 1000000', false)

WebUI.verifyOptionPresentByLabel(findTestObject('Object Repository/PPOB/Page_/select_nominal'), 'Rp 1000000', false, 60)

WebUI.click(findTestObject('PPOB/Page_/button_Topup'))

WebUI.click(findTestObject('PPOB/Page_Ladara/button_Pilih Metode Pembayaran'))

WebUI.click(findTestObject('PPOB/Page_Ladara/div_Bank Mandiri'))

WebUI.click(findTestObject('PPOB/Page_Ladara/button_Bayar Sekarang'))

WebUI.click(findTestObject('PPOB/Page_Ladara/Page_Ladara/a_DEV Simulasi pembayaran'))




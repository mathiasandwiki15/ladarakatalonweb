<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>PPOB</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0f75a763-aa4a-4c71-9197-34a80ac7d89e</testSuiteGuid>
   <testCaseLink>
      <guid>44bed187-367e-4450-99ea-0f2fcc815789</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PPOB/Gopay-BCA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>170caa28-e515-4692-aa2c-ac918f8dae7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PPOB/Gopay-BNI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4bbf40ac-074b-465b-a189-036f1e3b1028</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PPOB/Gopay-BRI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ee7056a-5ba5-4817-8181-bc02d7d0ce24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PPOB/Gopay-Mandiri</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3df84a0-49f1-452a-b965-189d9a275e5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PPOB/Gopay-Permata</testCaseId>
   </testCaseLink>
</TestSuiteEntity>

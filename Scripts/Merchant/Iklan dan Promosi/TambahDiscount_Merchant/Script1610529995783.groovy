import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.text.SimpleDateFormat as SimpleDateFormat
import java.util.Date as Date
import org.openqa.selenium.Keys as Keys

SimpleDateFormat date_format = new SimpleDateFormat('dd-MM-yyyy')

Date date = new Date()

Calendar c = Calendar.getInstance()

c.setTime(date)

c.add(Calendar.DATE, 1)

date = c.getTime()

Calendar d = Calendar.getInstance()

d.setTime(date)

d.add(Calendar.DATE, 30)

datee = d.getTime()

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/TambahVoucher_Merchant/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/TambahVoucher_Merchant/Page_Login/input_Selamat datang di Ladara_log'), 
    'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/TambahVoucher_Merchant/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/TambahVoucher_Merchant/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/TambahVoucher_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.navigateToUrl('https://staging.ladara.id/merchant/#/iklan-promosi')

WebUI.click(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/a_Discount'))

WebUI.setText(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input_namaDiscount'), 'Diskon Awal Tahun')

WebUI.setText(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input_Tanggal Mulai_date'), date_format.format(
        date))

WebUI.setText(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input_Tanggal Selesai_date'), 
    date_format.format(datee))

WebUI.setText(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input_Waktu Mulai_date'), '00:00')

WebUI.setText(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input_Waktu Selesai_date'), '00:00')

WebUI.click(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/button_Buat Diskon'))

WebUI.click(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/button_Tambah Produk'))

WebUI.check(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input'))

WebUI.acceptAlert()

WebUI.click(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input'))

WebUI.click(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/button_Konfirmasi'))

WebUI.setText(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input_Rp_discount'), '90000')

WebUI.setText(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/input_Diskon wajib diisi_pembelian_1'), 
    '5')

WebUI.click(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/button_Selesai'))

WebUI.click(findTestObject('Object Repository/TambahDiskon_Merchant/Page_Ladara Merchant/button_OK'))


<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Cart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>2b4d7ac6-3894-495c-8f68-a5ea18ca1a49</testSuiteGuid>
   <testCaseLink>
      <guid>df012f2e-2623-4142-988c-a806a1d58f69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/Add To Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20516cbf-18e8-416c-b0e8-164df4fe02b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/Get View Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1732bd18-2108-424d-9fa8-66304395385e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/Delete Cart</testCaseId>
   </testCaseLink>
</TestSuiteEntity>

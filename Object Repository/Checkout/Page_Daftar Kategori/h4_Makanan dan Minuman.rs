<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Makanan dan Minuman</name>
   <tag></tag>
   <elementGuidId>a29315bf-164d-4393-b5ce-61cca91f6862</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h4.tx_choosenCat</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h4[@class=&quot;tx_choosenCat&quot; and text()='Makanan dan Minuman']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>tx_choosenCat</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Makanan dan Minuman</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;post-5550&quot;)/div[@class=&quot;row row_globalPage&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;bx_chcat_2 bx_brandPage&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-4 col-md-2 col_bx_choosenCat col_branditems&quot;]/a[1]/h4[@class=&quot;tx_choosenCat&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//article[@id='post-5550']/div/div/div/div/div/a/h4</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar Kategori'])[2]/following::h4[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='UMKM'])[1]/preceding::h4[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kesehatan'])[1]/preceding::h4[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//article/div/div/div/div/div/a/h4</value>
   </webElementXpaths>
</WebElementEntity>

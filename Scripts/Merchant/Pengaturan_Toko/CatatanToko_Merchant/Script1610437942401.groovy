import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/AddToCart/Page_/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.click(findTestObject('PengirimanMerchant/Page_Ladara Merchant/b_Pengaturan Toko (1)'))

WebUI.click(findTestObject('Object Repository/Catatan Folder/Page_Ladara Merchant/div_Catatan'))

WebUI.click(findTestObject('Object Repository/Catatan Folder/Page_Ladara Merchant/input_Judul Catatan_form-control'))

WebUI.setText(findTestObject('Object Repository/Catatan Folder/Page_Ladara Merchant/input_Judul Catatan_form-control'), 
    'Catetan Toko 3')

def judul = WebUI.getText(findTestObject('Object Repository/Catatan Folder/Page_Ladara Merchant/input_Judul Catatan_form-control'))

WebUI.verifyMatch(judul, judul, true)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Catatan Folder/Page_Ladara Merchant/div_Isi Catatan_ql-editor ql-blank'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Catatan Folder/Page_Ladara Merchant/div_Isi Catatan_ql-editor ql-blank'), 
    'Catetan Isi Toko')

def isi = WebUI.getText(findTestObject('Object Repository/Catatan Folder/Page_Ladara Merchant/div_Isi Catatan_ql-editor ql-blank'))

WebUI.verifyMatch(isi, isi, true)

WebUI.delay(5)

WebUI.click(findTestObject('Information_Merchant/Page_Ladara Merchant/button_simpan_catatan'))

WebUI.closeBrowser()


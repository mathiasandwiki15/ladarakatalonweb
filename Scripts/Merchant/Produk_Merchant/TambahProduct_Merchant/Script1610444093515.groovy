import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/AddToCart/Page_/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_Produk Saya'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_Tambah Produk'))

WebUI.delay(5)

CustomKeywords.'newpackage.newkeywoard.uploadFile'(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/span_Gambar Utama'), 
    'C:\\Users\\lenovo\\git\\ladarakatalonweb\\gambarutama.jpg')

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_wajib_title'), 'Produk Testing')

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_wajib_form-control'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_UMKM'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_Baju Anak'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_Baju Anak'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/label_Brand                            wajib'))

WebUI.selectOptionByValue(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/select_AJP LABELAtagoAXIS-YBodyshopBrand Ba_272798'), 
    '5556', true)

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/textarea_wajib_description'), 
    'Deskripsi Produk')

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_Wajib_price'), '100000')

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_Wajib_stock'), '100')

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_Gunakan SKU untuk menambahkan kode un_5681fd'), 
    'SKU12')

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/input_Wajib_weight'), '250')

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_panjang'), "5")

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_lebar'), "5")

WebUI.delay(3)

WebUI.setText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Ukuran Produk (cm)_tinggi'), "5")

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/button_Simpan'))

WebUI.delay(3)

WebUI.closeBrowser()
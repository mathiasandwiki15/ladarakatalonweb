import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/AddToCart/Page_/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.click(findTestObject('Object Repository/TambahProduk_Merchant/Page_Ladara Merchant/div_Produk Saya'))

WebUI.click(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/div_Daftar Produk'))

WebUI.setText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Semua Produk_search-box-index'), 
    'Produk Testing')

WebUI.delay(5)

WebUI.clearText(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Semua Produk_search-box-index'))

WebUI.refresh()

WebUI.click(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/button_kategori'))

WebUI.check(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_MakananMinuman'))

WebUI.delay(5)

WebUI.uncheck(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_MakananMinuman'))

WebUI.delay(5)

WebUI.check(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_UMKM'))

WebUI.delay(5)

WebUI.uncheck(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_UMKM'))

WebUI.delay(5)

WebUI.check(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Kesehatan'))

WebUI.delay(5)

WebUI.uncheck(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Kesehatan'))

WebUI.delay(5)

WebUI.check(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Elektronik'))

WebUI.delay(5)

WebUI.uncheck(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Elektronik'))

WebUI.delay(5)

WebUI.check(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_LadaraEmas'))

WebUI.delay(5)

WebUI.uncheck(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_LadaraEmas'))

WebUI.delay(5)

WebUI.check(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Handphone'))

WebUI.delay(5)

WebUI.uncheck(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Handphone'))

WebUI.delay(5)

WebUI.check(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Atribut'))

WebUI.delay(5)

WebUI.uncheck(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Atribut'))

WebUI.delay(5)

WebUI.check(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Fashion'))

WebUI.delay(5)

WebUI.uncheck(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Fashion'))

WebUI.delay(5)

WebUI.check(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_PeralatanDapur'))

WebUI.delay(5)

WebUI.uncheck(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_PeralatanDapur'))

WebUI.delay(5)

WebUI.check(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Kecantikan'))

WebUI.delay(5)

WebUI.uncheck(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Kecantikan'))

WebUI.delay(5)

WebUI.check(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Hobi'))

WebUI.delay(5)

WebUI.uncheck(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/input_Hobi'))

WebUI.delay(5)

WebUI.refresh()

WebUI.click(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/button_Urutkan'))

WebUI.delay(5)

WebUI.mouseOver(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/a_Termahal'))

WebUI.check(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/a_Termahal'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/button_Urutkan'))

WebUI.delay(5)

WebUI.mouseOver(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/a_Termurah'))

WebUI.click(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/a_Termurah'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/button_Urutkan'))

WebUI.delay(5)

WebUI.mouseOver(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/a_Nama A - Z'))

WebUI.click(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/a_Nama A - Z'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/button_Urutkan'))

WebUI.delay(5)

WebUI.mouseOver(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/a_Nama Z - A'))

WebUI.click(findTestObject('Object Repository/DaftarProduct_Merchant/Page_Ladara Merchant/a_Nama Z - A'))

WebUI.delay(5)

WebUI.closeBrowser()


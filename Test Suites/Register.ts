<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Register</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>9dc918b3-7e3d-46a6-b878-ca740b3f8e92</testSuiteGuid>
   <testCaseLink>
      <guid>fab06e17-d463-4e00-be18-d3bacdb5b74c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Registration/RegisterFacebook</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>290fa4f1-60f4-43d7-857b-f0d13add21c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Registration/RegisterGoogle</testCaseId>
   </testCaseLink>
</TestSuiteEntity>

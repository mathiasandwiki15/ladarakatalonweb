<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Telor ceplok                                                                                                    Rp 105,000</name>
   <tag></tag>
   <elementGuidId>2728a168-862b-4adf-85dc-090b041ab149</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='container']/div[8]/div[5]/div/div[3]/div/a/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;Lihat Telor ceplok&quot;] > div.bx_in_productList</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>bx_in_productList</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                Telor ceplok
                                                                                                    Rp 105,000
                                                                                            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;container&quot;)/div[@class=&quot;row row_homePage&quot;]/div[@class=&quot;col-md-12 col_tabsPromo&quot;]/div[@class=&quot;row row_bnpromo&quot;]/div[@class=&quot;col-xs-6 col-md-3 col_bx_productList&quot;]/div[@class=&quot;bx_productList&quot;]/a[1]/div[@class=&quot;bx_in_productList&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='container']/div[8]/div[5]/div/div[3]/div/a/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 25,000'])[3]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Teman baik + sepatunya'])[3]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/a/div[2]</value>
   </webElementXpaths>
</WebElementEntity>

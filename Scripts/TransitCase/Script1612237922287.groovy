import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.Random as Random
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.text.SimpleDateFormat as SimpleDateFormat
import java.util.Date as Date

String nama = "efozecot-7275@yopmail.com"

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_/a_Masuk'))

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Login/input_Selamat datang di Ladara_log'), nama)

WebUI.setText(findTestObject('TambahVoucher_Merchant/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/div_Penjualan'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/div_Pesanan Baru'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/button_terimapesanan'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/button_yakin'))

WebUI.delay(5)

WebUI.closeBrowser()

// Melihat Status Pembelian

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.click(findTestObject('Object Repository/AddToCart/Page_/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(10)

WebUI.navigateToUrl('https://staging.ladara.id/my-order')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Status Pembelian/tab_Diproses'))

WebUI.verifyTextPresent("Diproses", false)

WebUI.delay(5)

// Penjual Request PickUp

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.click(findTestObject('Object Repository/AddToCart/Page_/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), nama)

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(10)

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/div_Penjualan'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Status Pembelian/tab_siapdikirim'))

WebUI.click(findTestObject('Object Repository/Status Pembelian/button_Request_pickup'))

WebUI.delay(5)

WebUI.closeBrowser()

// Pembeli Konfirmasi Pembelian

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.click(findTestObject('Object Repository/AddToCart/Page_/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), "mathias.brait@idstar.co.id")

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(10)

WebUI.navigateToUrl('https://staging.ladara.id/my-order')

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Status Pembelian/tab_dikirim'))

WebUI.delay(3)

WebUI.closeBrowser()

// Penjual Download Report Penjualan

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.click(findTestObject('Object Repository/AddToCart/Page_/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), nama)

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123%')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(10)

WebUI.click(findTestObject('TambahVoucher_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Penjualan_Merchant/Page_Ladara Merchant/div_Penjualan'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Status Pembelian/button_download_report_penjualan'))

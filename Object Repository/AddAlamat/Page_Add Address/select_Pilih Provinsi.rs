<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Pilih Provinsi</name>
   <tag></tag>
   <elementGuidId>ae0b71e8-4732-4a80-b956-6a0a37be47f3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='ch_province']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#ch_province</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>ch_province</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>provinsi</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sel_aform ch_province</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onchange</name>
      <type>Main</type>
      <value>get_kotaSel2(event)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            Pilih Provinsi
                                                                                            Aceh
                                                                                            Bali
                                                                                            Bangka Belitung
                                                                                            Banten
                                                                                            Bengkulu
                                                                                            DI Yogyakarta
                                                                                            DKI Jakarta
                                                                                            Gorontalo
                                                                                            Jambi
                                                                                            Jawa Barat
                                                                                            Jawa Tengah
                                                                                            Jawa Timur
                                                                                            Kalimantan Barat
                                                                                            Kalimantan Selatan
                                                                                            Kalimantan Tengah
                                                                                            Kalimantan Timur
                                                                                            Kalimantan Utara
                                                                                            Kepulauan Riau
                                                                                            Lampung
                                                                                            Maluku
                                                                                            Maluku Utara
                                                                                            new province
                                                                                            Nusa Tenggara Barat
                                                                                            Nusa Tenggara Timur
                                                                                            Papua
                                                                                            Papua Barat
                                                                                            Riau
                                                                                            Sulawesi Barat
                                                                                            Sulawesi Selatan
                                                                                            Sulawesi Tengah
                                                                                            Sulawesi Tenggara
                                                                                            Sulawesi Utara
                                                                                            Sumatera Barat
                                                                                            Sumatera Selatan
                                                                                            Sumatera Utara
                                                                                    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;ch_province&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='ch_province']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form_addAddress']/div[5]/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[5]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[6]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
   </webElementXpaths>
</WebElementEntity>

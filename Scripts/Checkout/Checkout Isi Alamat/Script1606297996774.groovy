import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://staging.ladara.id/login#!')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'testingkatalon13@gmail.com')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/Checkout/Page_/input_tutup_keyword'), "Bunga Penangkal Sial")

WebUI.click(findTestObject('Object Repository/Checkout/Page_/span_tutup_glyphicon glyphicon-search sp_searchNow submit-search'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/CheckoutBeforePayment/Page_Product/h4_Bunga Penangkal Sial'))

WebUI.click(findTestObject('Object Repository/CheckoutBeforePayment/Page_Jual Bunga Penangkal Sial/input_Sukses menambahkan produk ini ke keranjang belanja Anda._sub_buyProduct sub_addtoCart'))

WebUI.click(findTestObject('Checkout/Page_Cart/input_Rp 225,000_sub_cartCheckout'))

WebUI.click(findTestObject('Object Repository/Pengiriman/Page_Checkout/a_Pilih Alamat Lain'))

WebUI.click(findTestObject('CheckoutAlamat/Page_Jual Celana pendek anak Rcr -80 baby frozen/Page_Checkout/div_Tambah Alamat'))

WebUI.click(findTestObject('Object Repository/CheckoutAlamat/Page_Checkout/span_Atur Sebagai Alamat Utama_checkmark'))

WebUI.setText(findTestObject('Object Repository/CheckoutAlamat/Page_Checkout/input_Atur Sebagai Alamat Utama_nama_alamat'), 
    'Rumah Pribadi')

WebUI.setText(findTestObject('Object Repository/CheckoutAlamat/Page_Checkout/input_Atur Sebagai Alamat Utama_nama_lengkap'), 
    'Mathias Sitohang')

WebUI.setText(findTestObject('Object Repository/CheckoutAlamat/Page_Checkout/input_Atur Sebagai Alamat Utama_nomor_hp'), 
    '081286281318')

WebUI.setText(findTestObject('Object Repository/CheckoutAlamat/Page_Checkout/textarea_Atur Sebagai Alamat Utama_alamat'), 
    'Jalan Testing')

WebUI.selectOptionByLabel(findTestObject('Object Repository/CheckoutAlamat/Page_Checkout/select_Pilih Provinsi'), 'DKI Jakarta', 
    false)

WebUI.verifyOptionSelectedByLabel(findTestObject('Object Repository/CheckoutAlamat/Page_Checkout/select_Pilih Provinsi'), 
    'DKI Jakarta', false, 60)

WebUI.selectOptionByLabel(findTestObject('Dropdown Alamat/Page_Checkout/Page_Checkout/select_Pilih Kota'), 'Jakarta Timur', 
    false)

WebUI.verifyOptionPresentByLabel(findTestObject('Dropdown Alamat/Page_Checkout/Page_Checkout/select_Pilih Kota'), 'Jakarta Timur', 
    false, 60)

WebUI.selectOptionByLabel(findTestObject('Dropdown Alamat/Page_Checkout/Page_Checkout/select_Pilih Kecamatan'), 'Duren Sawit', 
    false)

WebUI.verifyOptionSelectedByLabel(findTestObject('Dropdown Alamat/Page_Checkout/Page_Checkout/select_Pilih Kecamatan'), 
    'Duren Sawit', false, 60)

WebUI.selectOptionByLabel(findTestObject('Dropdown Alamat/Page_Checkout/Page_Checkout/select_Pilih Kode Pos'), '13470 (Klender)', 
    false)

WebUI.verifyOptionSelectedByLabel(findTestObject('Dropdown Alamat/Page_Checkout/Page_Checkout/select_Pilih Kode Pos'), '13470 (Klender)', 
    false, 60)

WebUI.click(findTestObject('CheckoutAlamat/Page_Checkout/Page_Checkout/span_Alamat Utama_checkmark'))

WebUI.click(findTestObject('CheckoutAlamat/Page_Checkout/Page_Checkout/input_Gagal_sub_aform'))

WebUI.verifyTextPresent('Selamat!', false)

WebUI.closeBrowser()


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


//1. Pembeli melihat Deskripsi Produk - Spesifikasi Lengkap - Memberikan Diskusi 

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.click(findTestObject('Object Repository/AddToCart/Page_/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(10)

WebUI.navigateToUrl('https://staging.ladara.id/shop')

WebUI.click(findTestObject('Object Repository/UMKM/Page_PASAR UMKM - LaDaRa/Product_Sample/product_variant1'))

WebUI.click(findTestObject('Object Repository/UMKM/Page_PASAR UMKM - LaDaRa/pilih_varian_merah'))

WebUI.click(findTestObject('Object Repository/UMKM/Page_Jual DekranasFashionD/button_beliSekarang'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Checkout/Page_Cart/button_checkout'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Checkout/Page_Checkout/a_Pilih Alamat Lain'))

WebUI.click(findTestObject('Checkout/Page_Checkout/a_Pilih'))

WebUI.selectOptionByIndex(findTestObject('Object Repository/Checkout/Page_Checkout/Page_Checkout/select_shipping_method'),
	1, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Checkout/Page_Checkout/Page_Checkout/input_button_pilihpembayaran'))

WebUI.check(findTestObject('Object Repository/Checkout/Page_Payment/span_Kartu Kredit_ckm'))

WebUI.check(findTestObject('Object Repository/Checkout/Page_Payment/span_Syarat  Ketentuan_checkmark'))

WebUI.click(findTestObject('Object Repository/Checkout/Page_Payment/input_Syarat  Ketentuan_sub_cartCheckout mob_sub_paymentNow'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Checkout/Page_Checkout invoice-978/button_Klik disini untuk simulasi pembayara_9bca1a'))

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Checkout/Page_Checkout invoice-978/button_Bayar Sekarang'))

WebUI.delay(20)

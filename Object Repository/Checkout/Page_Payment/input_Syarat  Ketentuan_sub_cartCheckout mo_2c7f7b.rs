<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Syarat  Ketentuan_sub_cartCheckout mo_2c7f7b</name>
   <tag></tag>
   <elementGuidId>b0d890d3-e2bb-41d6-a42e-548ae9d6f022</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='Bayar Sekarang']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input.sub_cartCheckout.mob_sub_paymentNow</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sub_cartCheckout mob_sub_paymentNow</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Bayar Sekarang</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form_paynow&quot;)/div[@class=&quot;row row_masterCart&quot;]/div[@class=&quot;col-md-12 col_masterCart&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-5 col_checkout_page2&quot;]/div[@class=&quot;box_mainpay&quot;]/div[@class=&quot;box_listpay&quot;]/input[@class=&quot;sub_cartCheckout mob_sub_paymentNow&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='Bayar Sekarang']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form_paynow']/div/div/div[3]/div[2]/div/div[2]/input</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
   </webElementXpaths>
</WebElementEntity>

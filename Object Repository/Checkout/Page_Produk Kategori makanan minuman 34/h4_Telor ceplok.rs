<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Telor ceplok</name>
   <tag></tag>
   <elementGuidId>ba3178ad-78ae-49b4-a6cd-019f6d0123a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;Lihat Telor ceplok&quot;] > div.bx_in_productList > h4.ht_productList</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h4[@class=&quot;ht_productList&quot; and text()='Telor ceplok']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ht_productList</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Telor ceplok</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;view_resProduct&quot;)/div[@class=&quot;col-xs-6 col-md-4 col_bx_productList&quot;]/div[@class=&quot;bx_productList&quot;]/a[1]/div[@class=&quot;bx_in_productList&quot;]/h4[@class=&quot;ht_productList&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='view_resProduct']/div[22]/div/a/div[2]/h4</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 1,500,000'])[1]/following::h4[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 10,000'])[1]/preceding::h4[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Telor ceplok']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[22]/div/a/div[2]/h4</value>
   </webElementXpaths>
</WebElementEntity>

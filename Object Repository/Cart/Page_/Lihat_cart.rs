<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lihat_cart</name>
   <tag></tag>
   <elementGuidId>bb64ae8a-eef7-41d2-8e2e-c7f0fd063ea3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@href=&quot;https://staging.ladara.id/cart/&quot; and text()='Lihat keranjang' and @title=&quot;Lihat semua keranjang belanja Anda.&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://staging.ladara.id/')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/AddToCart/Page_/Page_/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Selamat datang di Ladara_log'), 'mathias.brait@idstar.co.id')

WebUI.setText(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Email_pwd'), 'Testing123')

WebUI.click(findTestObject('Object Repository/LoginObjectRepository/Page_Login/input_Masukkan Kata Sandi_user-submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Information_Merchant/Page_/span_maju bersamaaaaaa'))

WebUI.click(findTestObject('PengirimanMerchant/Page_Ladara Merchant/b_Pengaturan Toko (1)'))

WebUI.click(findTestObject('Object Repository/LokasiMerchant/Page_Ladara Merchant/div_Lokasi'))

WebUI.click(findTestObject('Object Repository/LokasiMerchant/Page_Ladara Merchant/input_Label Lokasi_form-control'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/LokasiMerchant/Page_Ladara Merchant/input_Label Lokasi_form-control'), 'Toko')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/LokasiMerchant/Page_Ladara Merchant/input_Alamat_form-control'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/LokasiMerchant/Page_Ladara Merchant/input_Alamat_form-control'), 'Komplek Depkes 3. Kalimalang , Bekasi')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/LokasiMerchant/Page_Ladara Merchant/input_Kecamatan_vs__search'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/LokasiMerchant/Page_Ladara Merchant/input_Kecamatan_vs__search'), 'Bekasi Selatan')

WebUI.sendKeys(findTestObject('LokasiMerchant/Page_Ladara Merchant/input_Kecamatan_vs__search'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/LokasiMerchant/Page_Ladara Merchant/input_Kode Pos_vs__search'), '17147')

WebUI.sendKeys(findTestObject('LokasiMerchant/Page_Ladara Merchant/input_Kode Pos_vs__search'), Keys.chord(Keys.ENTER))

WebUI.delay(5)

WebUI.setText(findTestObject('LokasiMerchant/Page_Ladara Merchant/input_detail_lokasi'), 'Komplek Depkes 3, Jalan Caman Raya, RT.009/RW.001, Jatibening, Kota Bekasi, Jawa Barat, Indonesia')

WebUI.sendKeys(findTestObject('LokasiMerchant/Page_Ladara Merchant/input_detail_lokasi'), Keys.chord(Keys.ENTER))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/LokasiMerchant/Page_Ladara Merchant/input_Telepon_form-control'))

WebUI.delay(5)

WebUI.setText(findTestObject('Object Repository/LokasiMerchant/Page_Ladara Merchant/input_Telepon_form-control'), '08988286288')

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/LokasiMerchant/Page_Ladara Merchant/b_Simpan'))

WebUI.delay(5)

WebUI.click(findTestObject('LokasiMerchant/Page_Ladara Merchant/button_OK'))

WebUI.closeBrowser()


<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0c7a09fc-3d87-4d3e-a975-5806f21969c1</testSuiteGuid>
   <testCaseLink>
      <guid>bb46ef20-0cf8-4b26-8285-bb13db9b43ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/LoginManual</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1d428760-e32a-4097-a897-c3ee65b9ffdd</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ExcelReader</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>1d428760-e32a-4097-a897-c3ee65b9ffdd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username </value>
         <variableId>9ff93325-7bcd-4df3-9566-4779958fe156</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1d428760-e32a-4097-a897-c3ee65b9ffdd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password </value>
         <variableId>763956bf-8b7b-44c9-aa0e-91664f368eae</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>

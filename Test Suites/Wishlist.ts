<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Wishlist</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>647c4da3-4c32-4f05-9b8e-8ccb388ca8f2</testSuiteGuid>
   <testCaseLink>
      <guid>41f90911-98c6-4a33-ab48-4bb2f0d623f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Wishlist/Add Product To Wishlist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>939ffceb-96b8-415d-88af-6ef71219af0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Wishlist/Get Product in Wishlist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b7f8504-978a-483f-b9a8-0e238dcfe51a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Wishlist/Delete Product in Wishlist</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
